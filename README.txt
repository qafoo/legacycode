### README

This is demo code to show legacy code refactorings. This is intentionally bad
code. Do not use for anything else.

## Installation

To install all required tools and libraries run:

    composer.phar install

Afterwards the build process can be triggered with:

    ant

The build process will use a running Solr and PHP internal webserver.

## Prerequisites

You must have a running Solr instance on the default port 8983. You can add the
default data to the index by running:

    ./bin/indexProducts

