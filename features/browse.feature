Feature: Browse legacy search website

    Scenario: Access Front page
        When I am on "/"
        Then I should see "Legacy Search"

    Scenario: Successfull search
       Given I am on "/"
        When I fill in "inputSearch" with "foo"
         And I press "inputSubmit"
        Then I should see "Product 0"

    Scenario: Search for non-existant search term
       Given I am on "/"
        When I fill in "inputSearch" with "non-existant"
         And I press "inputSubmit"
        Then I should see "No product found."
