<?php

namespace Acme;

abstract class Controller
{
    public function render($template, $variables)
    {
        $twig = new \Twig_Environment(
            new \Twig_Loader_Filesystem(__DIR__ . '/../templates/')
        );

        return new Response($twig->render($template, $variables));
    }

    public function generateUrl($route, $variables)
    {
        if ($route == 'search') {
            return '/search';
        }

        return '/product/' . $variables['name'];
    }
}
