<?php

namespace Acme\Controller;

use Acme\Controller;
use Acme\Request;
use Acme\Response\JsonResponse;

class SearchController extends Controller
{
    /**
     * @param Request $request
     *
     * @return Response
     */
    public function searchAction(Request $req)
    {
        if ($req->has('q')) {
            $solarium = new \Solarium\Client(array(
                'endpoint' => array(
                    'localhost' => array(
                        'host' => '127.0.0.1',
                        'port' => 8983,
                        'path' => '/solr/',
                    )
                )
            ));
            $select = $solarium->createSelect();

            // configure dismax
            $dismax = $select->getDisMax();
            $dismax->setQueryFields(array('name^2', 'description'));
            $dismax->setPhraseFields(array('description'));
            $dismax->setMinimumMatch(1);
            $dismax->setQueryParser('edismax');

            if ($req->has('q')) {
                $escapedQuery = $select->getHelper()->escapeTerm($req->get('q'));
                $select->setQuery($escapedQuery);
            }

            $paginator = new \Pagerfanta\Pagerfanta(new \Pagerfanta\Adapter\SolariumAdapter($solarium, $select));
            $paginator->setMaxPerPage(15);
            $paginator->setCurrentPage($req->get('page', 1), false, true);

            if ($req->getRequestFormat() === 'json') {
                try {
                    $result = array(
                        'results' => array(),
                        'total' => $paginator->getNbResults(),
                    );
                } catch (\Solarium_Client_HttpException $e) {
                    return new JsonResponse(array(
                        'status' => 'error',
                        'message' => 'Could not connect to the search server',
                    ), 500);
                }

                foreach ($paginator->getIterator() as $product) {
                    $url = $this->generateUrl('view_product', array('name' => $product->name), true);

                    $result['results'][] = array(
                        'name' => $product->name,
                        'description' => $product->description ?: '',
                        'price' => number_format($product->price, 2),
                        'vat' => number_format($product->price - $product->price / 1.19, 2),
                        'url' => $url,
                    );
                }

                if ($paginator->hasNextPage()) {
                    $params = array(
                        '_format' => 'json',
                        'q' => $req->get('q'),
                        'page' => $paginator->getNextPage()
                    );
                    $result['next'] = $this->generateUrl('search', $params, true);
                }

                return new JsonResponse($result);
            }

            if ($req->isXmlHttpRequest()) {
                try {
                    return $this->render('SearchBundle:Web:list.html.twig', array(
                        'products' => iterator_to_array($paginator->getIterator()),
                        'noLayout' => true,
                    ));
                } catch (\Twig_Error_Runtime $e) {
                    if (!$e->getPrevious() instanceof \Solarium_Client_HttpException) {
                        throw $e;
                    }
                    return new JsonResponse(array(
                        'status' => 'error',
                        'message' => 'Could not connect to the search server',
                    ), 500);
                }
            }

            return $this->render('SearchBundle:Web:search.html.twig', array(
                'products' => $paginator,
            ));
        } elseif ($req->getRequestFormat() === 'json') {
            return new JsonResponse(array('error' => 'Missing search query, example: ?q=example'), 400);
        }

        return $this->render('SearchBundle:Web:search.html.twig', array());
    }

    public function indexAction(Request $req)
    {
        return $this->render('SearchBundle:Web:index.html.twig', array());
    }

    public function productAction(Request $req)
    {
        return $this->render('SearchBundle:Web:product.html.twig', array());
    }
}
