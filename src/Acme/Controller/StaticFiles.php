<?php

namespace Acme\Controller;

use Acme\Controller;
use Acme\Request;
use Acme\Response;

class StaticFiles extends Controller
{
    private $extensionMap = array(
        'css' => 'text/css',
        'js' => 'text/javascript',
    );

    public function serveAction(Request $req)
    {
        $baseDir = realpath(__DIR__ . '/../../../web/');
        $fileName = realpath($baseDir . $_SERVER['REQUEST_URI']);

        if (strpos($fileName, $baseDir) !== 0) {
            throw new \RuntimeException("Invalid file requested.");
        }

        $extension = pathinfo($fileName, \PATHINFO_EXTENSION);
        header('Content-Type: ' . $this->extensionMap[$extension]);
        return new Response(file_get_contents($fileName));
    }
}
