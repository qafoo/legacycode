<?php

namespace Acme;

class Request
{
    private $variables = array();
    private $requestFormat = 'html';

    public function __construct(array $variables = array())
    {
        $this->variables = $variables;
    }

    public function has($name)
    {
        return isset($this->variables[$name]);
    }

    public function get($name, $defaultValue = null)
    {
        if (!$this->has($name)) {
            return $defaultValue;
        }

        return $this->variables[$name];
    }

    public function set($name, $value)
    {
        $this->variables[$name] = $value;
    }

    public function getRequestFormat()
    {
        return $this->requestFormat;
    }

    public function setRequestFormat($format)
    {
        $this->requestFormat = $format;
    }

    public function isXmlHttpRequest()
    {
        return false;
    }
}
