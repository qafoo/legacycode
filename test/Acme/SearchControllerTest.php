<?php

namespace Acme\Controller;

use Acme\Request;

class SearchControllerTest extends \PHPUnit_Framework_TestCase
{
    public function testController()
    {
        $ctrl = new SearchController();
        $response = $ctrl->searchAction(new Request());
    }

    public function testControllerWithSearch()
    {
        $request = new Request();
        $request->set('q', 'Hello');

        $ctrl = new SearchController();
        $response = $ctrl->searchAction($request);
    }

    public function testControllerWithJsonSearch()
    {
        $request = new Request();
        $request->set('q', 'Hello');
        $request->setRequestFormat('json');

        $ctrl = new SearchController();
        $response = $ctrl->searchAction($request);
    }
}
