<?php

require_once __DIR__ . '/../vendor/autoload.php';

$request = new Acme\Request($_GET);

$searchController = new Acme\Controller\SearchController();
switch (true) {
    case preg_match('(^/$)', $_SERVER['REQUEST_URI']):
        $response = $searchController->indexAction($request);
        break;
    case preg_match('(^/search)', $_SERVER['REQUEST_URI']):
        $response = $searchController->searchAction($request);
        break;
    case preg_match('(^/product)', $_SERVER['REQUEST_URI']):
        $response = $searchController->productAction($request);
        break;
    case preg_match('(^/assets)', $_SERVER['REQUEST_URI']):
        $staticController = new Acme\Controller\StaticFiles();
        $response = $staticController->serveAction($request);
        break;
    default:
        throw new \RuntimeException("No route found for " . $_SERVER['REQUEST_URI']);
}

echo $response->getContent();
